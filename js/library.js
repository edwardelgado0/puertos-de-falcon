var animationSpeed = 750;
var library = [];

$(document).ready(function(){
    fillLibrary();
    attachAnimations();    
});

/* -----------------------------------------------------------------------------
    FILL PAGE HTML 
   ---------------------------------------------------------------------------*/
function fillLibrary() {
    assembleData();
    var classlist = ['left-side first','left-side','right-side'];
        for (i=0; i < library.length; i++) {
            var book = library[i];
            // add html for current book
            var html = '<li class="book ' + classlist[0] + '">';
            html += '<div class="cover"><a href="'+book.url+'"><img src="' + book.cover + '" /></a></div>';
            html += '<div class="summary">';
            html += '<h1>' + book.title + '</h1>';
            html += '<h2>by ' + book.author + '</h2>';
            html += '<p>' + book.abstract + '</p>';
            html += '</div><span class="tooltiptext">'+book.title+'</span></li>';
            $('.library').append(html);
            // shift the classlist array for the next iteration
            var cn = classlist.shift();
            classlist.push(cn);
        }
   
}
/* -----------------------------------------------------------------------------
    ANIMATION 
   ---------------------------------------------------------------------------*/
   /*
function attachAnimations() {
    $('.book').click(function(){
        if (!$(this).hasClass('selected')) {
            selectAnimation($(this));
        }
    });
    $('.book .cover').click(function(){
        if ($(this).parent().hasClass('selected')) {
           deselectAnimation($(this).parent());
        }
    });
}
    */
function selectAnimation(obj) {
    obj.addClass('selected');
    // elements animating
    var cover = obj.find('.cover');
    var image = obj.find('.cover img');
    var library = $('.library');
    var summaryBG = $('.overlay-summary');
    var summary = obj.find('.summary');
    // animate book cover
    cover.animate({
        width: '280px',
        height: '448px' 
    }, {
        duration: animationSpeed
    });
    image.animate({
        width: '280px',
        height: '448px',
        borderWidth: '10px'
    },{
        duration: animationSpeed
    });
    // add fix if the selected item is in the bottom row
    if (isBtmRow()){
      library.css('paddingBottom','234px');
    }
    // slide page so book always appears
    positionTop();
    // add background overlay
    $('.overlay-page').show();
    // locate summary overlay    
    var px = overlayVertPos();
    summaryBG.css('left',px);
    // animate summary elements
    var ht = $('.content').height();
    var pos = $('.book.selected').position();
    var start = pos.top + 30; // 10px padding-top on .book + 20px padding of .summary
    var speed = Math.round((animationSpeed/ht) * 450); // 450 is goal height
    summaryBG.show().animate({
        height: ht + 'px'
    },{
        duration: animationSpeed,
        easing: 'linear',
        step: function(now,fx){
            if (now > start && fx.prop === "height"){
                if(!summary.is(':animated') && summary.height() < 450){
                    summary.show().animate({
                        height: '450px'
                    },{
                        duration: speed,
                        easing: 'linear'
                    });
                }
                
            }
        } 
        
    });
}

function deselectAnimation(obj) {
    // elements animating
    var cover = obj.find('.cover');
    var image = obj.find('.cover img');
    var library = $('.library');
    var summaryBG = $('.overlay-summary');
    var summary = obj.find('.summary');
    // stop summary animation
    summary.stop();
    // animate book cover
    cover.stop().animate({
        width: '140px',
        height: '224px' 
    },{
        duration:animationSpeed
    });
    image.stop().animate({
        width: '140px',
        height: '224px',
        borderWidth: '0px'
    },{
        duration: animationSpeed,
        complete: function() {
            obj.removeClass('selected');
        }
    });
    // remove fix for bottom row, if present
    library.stop().animate({
        paddingBottom:'10px'
    },{ 
        duration: animationSpeed
    });
    // remove background overlay and summary
    var ht = summaryBG.height();
    var pos = $('.book.selected').position();
    var start = pos.top + 480; //10px of top padding + 470px for .summary height + padding
    var speed = Math.round((animationSpeed/ht) * summary.height());
    summaryBG.stop().animate({
        height: '0px'
    },{
        duration: animationSpeed,
        easing: 'linear',
        step: function(now,fx){
            if (now < start && fx.prop === "height"){
                if(!summary.is(':animated') && summary.height() > 0){
                    summary.animate({
                        height: '0px'
                    },{ 
                        duration: speed,
                        easing: 'linear',
                        complete: function(){
                            summary.hide(); 
                        }
                    });
                }
                
            }
        }, 
        complete: function(){
            $('.overlay-page').hide();
            summary.hide(); // catching this twice to insure for aborted animation
            summaryBG.hide();
        }
    });
}

function isBtmRow() {
    var pos = $('.book.selected').position();
    var libHgt = $('.content').height();
    if (libHgt-pos.top===254) { // this is current height of the book, plus 30 for padding on the book and library
        return true;
    } else {
        return false;
    }
}

function positionTop() { 
   var offset = $('.book.selected').offset();
   var bTop = offset.top;
   $('html, body').animate({ scrollTop: bTop }, animationSpeed);
}

function overlayVertPos() { // determines the vertical position for the summary overlay based on selection position
    var pos = $('.book.selected').position();
    switch(pos.left) {
        case 0:
            return '320px';
        case 160:
            return '320px';
        case 320:
            return '320px';
        case 480:
            return '0px';
        case 640:
            return '320px';
        case 800:
            return '320px';
        default:
            return false;
    }
}
/* -----------------------------------------------------------------------------
    BUILD LIBRARY ARRAY 
   ---------------------------------------------------------------------------*/
function Book(cover,title,author,abstract,url) {
    this.cover = cover;
    this.title = title;
    this.author = author;
    this.abstract = abstract;
    this.url = url;
    library.push(this);
}

function assembleData() {
    var book;
    book = new Book('img/leyes/contitucion.png','CONSTITUCION DE LA REPUBLICA BOLIVARIANA DE VENEZUELA','author','description;','img/pdf/CONSTITUCION_DE_LA REPUBLICA_BOLIVARIANA_DE_VENEZUELA.pdf');
    book = new Book('img/leyes/LEY_DEL_IAPPEF.png','LEY DEL IAPPEF','author','description','img/pdf/LEY_DEL_IAPPEF.pdf');
    book = new Book('img/leyes/ley_general_de_puerto.png','LEY GENERAL DE PUERTOS','author','description','img/pdf/ley_general_de_puerto.pdf');
    book = new Book('img/leyes/LOTTT.png','LEY ORGÁNICA DEL TRABAJO, LOS TRABAJADORES Y LAS TRABAJADORAS','author','description','img/pdf/LOTTT.pdf');
    book = new Book('img/leyes/LEY_DEL_ESTATUTO_DE_LA_FUNCION_PUBLICA.png','LEY DEL ESTATUTO DE LA FUNCION PUBLICA','author','description','img/pdf/LEY_DEL_ESTATUTO_DE_LA_FUNCION_PUBLICA.pdf');
    book = new Book('img/leyes/LEY_DE_TASAS.png','LEY DE TASAS','author','description','img/pdf/LEY_DE_TASAS.pdf');
    book = new Book('img/leyes/LEY_ORGANICA_DE_ADUANA.png','LEY ORGANICA DE ADUANAS','author','description','img/pdf/LEY_ORGANICA_DE_ADUANA.pdf');
    book = new Book('img/leyes/REGLAMENTO_LEY_ORGANICA_DE_ADUANA.png','REGLAMENTO LEY ORGANICA DE ADUANAS','author','description','img/pdf/REGLAMENTO_LEY_ORGANICA_DE_ADUANA.pdf');
    book = new Book('img/leyes/REGLAMENTO_INTERNO_IAPPEF2.png','REGLAMENTO INTERNO IAPPEF','author','description','img/pdf/REGLAMENTO_INTERNO_IAPPEF2.pdf');
    book = new Book('img/leyes/REGLAMENTO_INTERNO_DE_ARCHIVO_DE_IAPPEF.png','REGLAMENTO DE ARCHIVO DE IAPPEF','author','description','img/pdf/REGLAMENTO_INTERNO_DE_ARCHIVO_DE_IAPPEF.pdf');
    book = new Book('img/leyes/REGLAMENTO_INTERNO_DE_DONACIONES.png','REGLAMENTO DE DONACIONES','author','description','img/pdf/REGLAMENTO_INTERNO_DE_DONACIONES.pdf');
    book = new Book('img/leyes/Ley_Orgánica_de_Bienes_Publicos.png','Ley Orgánica de Bienes Públicos','author','description','img/pdf/Ley_Orgánica_de_Bienes_Publicos.pdf');
    book = new Book('img/leyes/REGLAMENTO_DE_LA LEY_ORGANICA_DE_LA_CONTRALORIA_GENERAL_DE_LA_REPUBLICA.png','REGLAMENTO DE LA LEY ORGANICA DE LA CONTRALORIA GENERAL DE LA REPUBLICA','author','description','img/pdf/REGLAMENTO_DE_LA_LEY_ORGANICA_DE_LA_CONTRALORIA_GENERAL_DE_LA_REPUBLICA.pdf');
    book = new Book('img/leyes/REQUISITOS_INSCRIPCION_PERSONAS_NATURALES.png','REQUISITOS INSCRIPCION PERSONAS NATURALES','author','description','img/pdf/REQUISITOS_INSCRIPCION_PERSONAS_NATURALES.pdf');
}