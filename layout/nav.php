 <nav>
<a id="resp-menu" class="responsive-menu" href="#"><i class="fa fa-reorder"></i> Menu</a>    
   <ul class="menu">
   <li><a class="homer" href="index.html"><i class="fa fa-home"></i> INICIO</a></li>
  <li><a  href="#">QUIENES SOMOS</a>
    <ul class="sub-menu">
       <li><a href="reseñahistorica.html">Reseña Historica</a></li>
       <li><a href="mision-vision.html">Misión y Visión</a></li> 
   </ul>
  </li>
  <li><a  href="#">NUESTROS PUERTOS</a>
  <ul class="sub-menu">
   <li><a href="guaranao.html">Puerto de Guaranao</a></li>
   <li><a href="zazarida.html">Puerto de zazárida</a>
   <li><a href="muaco.html">Puerto de Muaco</a>
   <li><a href="piedras.html">Puerto de Las Piedras</a>
   <li><a href="cumarebo.html">Puerto Cumarebo</a></li>
  </ul>
  </li>
  <li><a  href="#">SERVICIOS</a>
    <ul>
      <li><a href="almafalca.html">Almafalca</a>
        <ul>
          <li><a href="almafalca_mision.html">Misión y Visión</a></li>
          <li><a href="almafalca_servicios.html">Servicios</a></li>
        </ul>
      </li>
      <li><a href="#">AIP</a></li>
    </ul>
  </li>
  <li><a  href="#">TRÁMITES</a>
  <ul class="sub-menu">
   <li><a href="#">Juridicos</a></li>
   <li><a href="img/pdf/pcp.pdf">PCP</a></li>
   </ul>
  </li>

  <li><a  href="#">MARCO LEGAL</a>
    <ul>
      <li><a href="leyesyreglamentos.html">Leyes y Reglamentos</a></li>
    </ul>
  </li>

  <li><a  href="#">CONTÁCTANOS</a></li>
  </ul>
  </nav>
